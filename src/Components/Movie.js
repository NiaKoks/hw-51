import React from "react";

const Movie = props =>{
    return(
        <div className="Movie_card">
            <h2 className="title">{props.title}</h2>
            <img className="poster" src={props.img} alt=""/>
            <p>Year: {props.year}</p>
        </div>
    )
}

export default Movie;