import React from "react";

const ContentBox = props =>{
    return(
        <div className="content">
            {props.children}
        </div>
    )
}

export default ContentBox;