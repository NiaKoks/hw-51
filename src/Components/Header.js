import React from "react";

const HeaderBox = props =>{
    return(
        <div className="header">
            <h1>{props.site_name}</h1>
        </div>
    )
}
export default HeaderBox;